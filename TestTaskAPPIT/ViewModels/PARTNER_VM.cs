﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestTaskAPPIT.Models;

namespace TestTaskAPPIT.ViewModels
{
    public class PARTNER_VM
    {
        public PARTNER PARTNER { get; set; }
        public decimal TeamPurchase { get; set; }
        public decimal TotalPurchase { get; set; }
        public decimal OwnBonusPurchase { get; set; }
        public decimal TeamBonusPurchase { get; set; }
        public decimal TotalCommission { get; set; }
    }
}
