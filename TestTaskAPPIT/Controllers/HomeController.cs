﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using TestTaskAPPIT.Models;
using TestTaskAPPIT.ViewModels;

namespace TestTaskAPPIT.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View(Helper.items);
        }
        [HttpGet]
        public ActionResult Create()
        {
            var model = new FINANCIAL_ITEM
            {
                DATE = DateTime.Now,
                PARTNERS = Helper.partners
                    .Select(x => new SelectListItem { Value = x.PARTNER_ID.ToString(), Text = x.PARTNER_NAME })
                    .ToList()
            };
            return View(model);
        }
        [HttpPost]
        public ActionResult Create(FINANCIAL_ITEM model)
        {
            if (!ModelState.IsValid)
            {
                model = new FINANCIAL_ITEM
                {
                    DATE = DateTime.Now,
                    PARTNERS = Helper.partners
                        .Select(x => new SelectListItem { Value = x.PARTNER_ID.ToString(), Text = x.PARTNER_NAME })
                        .ToList()
                };
            }

            model.PARTNER = Helper.partners
                .Where(x => x.PARTNER_ID == model.PARTNER_ID)
                .FirstOrDefault();

            model.FINANCIAL_ITEM_ID = Guid.NewGuid();
            Helper.items.Add(model);

            return RedirectToAction("Index");
        }
        [HttpGet]
        public ActionResult Edit(Guid id)
        {
            var data = Helper.items.Where(x => x.FINANCIAL_ITEM_ID == id).FirstOrDefault();
            data.PARTNERS = Helper.partners
                .Select(x => new SelectListItem { Value = x.PARTNER_ID.ToString(), Text = x.PARTNER_NAME })
                .ToList();

            return View(data);
        }
        [HttpPost]
        public ActionResult Edit(FINANCIAL_ITEM model)
        {
            if (!ModelState.IsValid)
            {
                model = new FINANCIAL_ITEM
                {
                    DATE = DateTime.Now,
                    PARTNERS = Helper.partners
                        .Select(x => new SelectListItem { Value = x.PARTNER_ID.ToString(), Text = x.PARTNER_NAME })
                        .ToList()
                };
            }

            var data = Helper.items.Where(x => x.FINANCIAL_ITEM_ID == model.FINANCIAL_ITEM_ID).FirstOrDefault();
            if (data != null)
            {
                data.PARTNER_ID = model.PARTNER_ID;
                data.AMOUNT = model.AMOUNT;
                data.DATE = model.DATE;
            }
            return RedirectToAction("Index");
        }
        [HttpGet]
        public ActionResult Delete(Guid id)
        {
            var data = Helper.items
                .Where(x => x.FINANCIAL_ITEM_ID == id)
                .FirstOrDefault();

            Helper.items.Remove(data);

            return RedirectToAction("Index");
        }
        public IActionResult PartnerCalculation()
        {
            var partnerCalculations = Helper.CalculationPartners();
            return View(partnerCalculations);
        }
    }
}
