#pragma checksum "C:\Users\xUser\Desktop\TestTaskAPPIT\TestTaskAPPIT\Views\Home\PartnerCalculation.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "90f5ce8ff25840af338c778b5e74cc594d6feaa6"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Home_PartnerCalculation), @"mvc.1.0.view", @"/Views/Home/PartnerCalculation.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "C:\Users\xUser\Desktop\TestTaskAPPIT\TestTaskAPPIT\Views\_ViewImports.cshtml"
using TestTaskAPPIT;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Users\xUser\Desktop\TestTaskAPPIT\TestTaskAPPIT\Views\_ViewImports.cshtml"
using TestTaskAPPIT.Models;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"90f5ce8ff25840af338c778b5e74cc594d6feaa6", @"/Views/Home/PartnerCalculation.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"55711a368b81434efa35f8240eb275415777534f", @"/Views/_ViewImports.cshtml")]
    public class Views_Home_PartnerCalculation : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<List<TestTaskAPPIT.ViewModels.PARTNER_VM>>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-action", "Index", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("class", new global::Microsoft.AspNetCore.Html.HtmlString("btn btn-outline-primary"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_2 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("style", new global::Microsoft.AspNetCore.Html.HtmlString("margin-bottom:10px"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        #pragma warning restore 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#nullable restore
#line 2 "C:\Users\xUser\Desktop\TestTaskAPPIT\TestTaskAPPIT\Views\Home\PartnerCalculation.cshtml"
  
    ViewBag.Title = "Calculation Page";

#line default
#line hidden
#nullable disable
            WriteLiteral("<div class=\"row\">\r\n    ");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("a", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "90f5ce8ff25840af338c778b5e74cc594d6feaa64414", async() => {
                WriteLiteral("Return to List");
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Action = (string)__tagHelperAttribute_0.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_0);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_1);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_2);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral(@"
    <table class=""table table-bordered table-hover"">
        <thead>
            <tr>
                <th>ID </th>
                <th>PARENT ID </th>
                <th>Partner </th>
                <th>FEE </th>
                <th>Team Purchase </th>
                <th>Total Purchase</th>
                <th>Own Bonus Purchase</th>
                <th>Team Bonus Purchase</th>
                <th>Total Commission </th>
            </tr>
        </thead>
        <tbody>
");
#nullable restore
#line 22 "C:\Users\xUser\Desktop\TestTaskAPPIT\TestTaskAPPIT\Views\Home\PartnerCalculation.cshtml"
             foreach (var d in Model)
            {

#line default
#line hidden
#nullable disable
            WriteLiteral("            <tr>\r\n                <td>");
#nullable restore
#line 25 "C:\Users\xUser\Desktop\TestTaskAPPIT\TestTaskAPPIT\Views\Home\PartnerCalculation.cshtml"
               Write(d.PARTNER.PARTNER_ID);

#line default
#line hidden
#nullable disable
            WriteLiteral("</td>\r\n                <td>");
#nullable restore
#line 26 "C:\Users\xUser\Desktop\TestTaskAPPIT\TestTaskAPPIT\Views\Home\PartnerCalculation.cshtml"
               Write(d.PARTNER.PARENT_PARTNER_ID);

#line default
#line hidden
#nullable disable
            WriteLiteral("</td>\r\n                <td>");
#nullable restore
#line 27 "C:\Users\xUser\Desktop\TestTaskAPPIT\TestTaskAPPIT\Views\Home\PartnerCalculation.cshtml"
               Write(d.PARTNER.PARTNER_NAME);

#line default
#line hidden
#nullable disable
            WriteLiteral("</td>\r\n                <td>");
#nullable restore
#line 28 "C:\Users\xUser\Desktop\TestTaskAPPIT\TestTaskAPPIT\Views\Home\PartnerCalculation.cshtml"
               Write(d.PARTNER.FEE_PERCENT);

#line default
#line hidden
#nullable disable
            WriteLiteral("</td>\r\n                <td>");
#nullable restore
#line 29 "C:\Users\xUser\Desktop\TestTaskAPPIT\TestTaskAPPIT\Views\Home\PartnerCalculation.cshtml"
               Write(d.TeamPurchase);

#line default
#line hidden
#nullable disable
            WriteLiteral("</td>\r\n                <td>");
#nullable restore
#line 30 "C:\Users\xUser\Desktop\TestTaskAPPIT\TestTaskAPPIT\Views\Home\PartnerCalculation.cshtml"
               Write(d.TotalPurchase);

#line default
#line hidden
#nullable disable
            WriteLiteral("</td>\r\n                <td>");
#nullable restore
#line 31 "C:\Users\xUser\Desktop\TestTaskAPPIT\TestTaskAPPIT\Views\Home\PartnerCalculation.cshtml"
               Write(d.OwnBonusPurchase);

#line default
#line hidden
#nullable disable
            WriteLiteral("</td>\r\n                <td>");
#nullable restore
#line 32 "C:\Users\xUser\Desktop\TestTaskAPPIT\TestTaskAPPIT\Views\Home\PartnerCalculation.cshtml"
               Write(d.TeamBonusPurchase);

#line default
#line hidden
#nullable disable
            WriteLiteral("</td>\r\n                <td>");
#nullable restore
#line 33 "C:\Users\xUser\Desktop\TestTaskAPPIT\TestTaskAPPIT\Views\Home\PartnerCalculation.cshtml"
               Write(d.TotalCommission);

#line default
#line hidden
#nullable disable
            WriteLiteral("</td>\r\n            </tr>\r\n");
#nullable restore
#line 35 "C:\Users\xUser\Desktop\TestTaskAPPIT\TestTaskAPPIT\Views\Home\PartnerCalculation.cshtml"
            }

#line default
#line hidden
#nullable disable
            WriteLiteral("        </tbody>\r\n    </table>\r\n</div>\r\n\r\n\r\n");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<List<TestTaskAPPIT.ViewModels.PARTNER_VM>> Html { get; private set; }
    }
}
#pragma warning restore 1591
