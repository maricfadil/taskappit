﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace TestTaskAPPIT.Models
{
    public class FINANCIAL_ITEM
    {
        public Guid FINANCIAL_ITEM_ID { get; set; }
        public decimal PARTNER_ID { get; set; }
        public PARTNER PARTNER { get; set; }
        public DateTime DATE { get; set; }
        public decimal AMOUNT { get; set; }
        public List<SelectListItem> PARTNERS { get; set; }
    }
}
