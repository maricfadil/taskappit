﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestTaskAPPIT.Models
{
    public class PARTNER
    {
        public decimal PARTNER_ID { get; set; }
        public string PARTNER_NAME { get; set; }
        public decimal? PARENT_PARTNER_ID { get; set; }
        public decimal FEE_PERCENT { get; set; }
    }
}
