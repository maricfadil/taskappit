﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestTaskAPPIT.Models;
using TestTaskAPPIT.ViewModels;

namespace TestTaskAPPIT
{
    public class Helper
    {
        public static List<FINANCIAL_ITEM> items = new List<FINANCIAL_ITEM>();
        public static List<PARTNER> partners = new List<PARTNER>();
        public static List<PARTNER> GeneratePartners(int numberOfPartners)
        {
            var random = new Random();
            var partners = new List<PARTNER>();
            for (int i = 1; i <= numberOfPartners; i++)
            {
                var partner = new PARTNER();
                partner.PARTNER_ID = i;
                partner.PARTNER_NAME = string.Format("Partner {0}", i);
                partner.FEE_PERCENT = random.Next(1, 20);
                if (i > 3 && i < 7)
                {
                    partner.PARENT_PARTNER_ID = random.Next(1, 3);
                }
                if (i >= 7)
                {
                    partner.PARENT_PARTNER_ID = random.Next(4, 7);
                }
                partners.Add(partner);
            }
            return partners;
        }
        public static List<PARTNER_VM> CalculationPartners()
        {
            var partnerCalculations = new List<PARTNER_VM>();
            foreach (var partner in partners)
            {
                var partnerCalculation = new PARTNER_VM();
                var teamPurchase = GetTeamPurchase(partner.PARTNER_ID);
                var ownPurchase = items
                    .Where(x => x.PARTNER_ID == partner.PARTNER_ID)
                    .Sum(y => y.AMOUNT);
                partnerCalculation.TeamPurchase = teamPurchase;
                partnerCalculation.TotalPurchase = ownPurchase + teamPurchase;
                partnerCalculation.OwnBonusPurchase = Math.Round((ownPurchase * partner.FEE_PERCENT) / 100,2);
                partnerCalculation.TeamBonusPurchase = Math.Round((teamPurchase * partner.FEE_PERCENT) / 100, 2);
                partnerCalculation.TotalCommission = Math.Round(partnerCalculation.TotalPurchase > 0 ? (partnerCalculation.TotalPurchase / (partnerCalculation.OwnBonusPurchase + partnerCalculation.TeamBonusPurchase)) : 0, 2);
                partnerCalculation.PARTNER = partner;
                partnerCalculations.Add(partnerCalculation);
            }
            return partnerCalculations;
        }
        private static decimal GetTeamPurchase(decimal partnerId)
        {
            var teamPartners = new List<PARTNER>();
            var parentPartnerIDs = new List<decimal> { partnerId };
            var items = new List<PARTNER>();

            do
            {
                items = partners
                    .Where(x => x.PARENT_PARTNER_ID != null
                           && parentPartnerIDs.Contains((decimal)x.PARENT_PARTNER_ID))
                    .ToList();
                parentPartnerIDs = items.Select(x => x.PARTNER_ID).ToList();
                teamPartners.AddRange(items);
            }
            while (items.Count() > 0);

            var teamPartnersIDs = teamPartners.Select(x => x.PARTNER_ID).ToList();
            var teamPurchase = Helper.items
                .Where(x => teamPartnersIDs.Contains(x.PARTNER_ID))
                .Sum(y => y.AMOUNT);

            return teamPurchase;
        }
    }
}
